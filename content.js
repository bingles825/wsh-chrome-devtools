/** This content script loads an ImageOverlay.js module. */

'use strict'

localStorage.setItem('WSH_EXTENSION_ROOT', chrome.runtime.getURL('/'))

// Our overlay app needs an element to attach to
const div = document.createElement('div')
div.id = 'wsh-content-script-app'
document.body.appendChild(div)

injectJsModule('app/ImageOverlay.js')

/**
 * Chrome extensions don't support setting type="module" for scripts loaded via
 * the manifest.json, so we have to inject them manually.
 */
function injectJsModule(path) {
  // script tag of type="module"
  const script = document.createElement('script')
  script.setAttribute('type', 'module')
  script.setAttribute('src', chrome.runtime.getURL(path))

  document.head.appendChild(script)
}
