import htm from '../lib/htm.module.js';

// @ts-ignore
export const html = htm.bind(React.createElement);

const urlMatch = /(GetObject|GetData)/;

export function sendDataToContentScript(data) {
  const queryOptions = { active: true, currentWindow: true };
  chrome.tabs.query(queryOptions).then(([tab]) => {
    chrome.tabs.sendMessage(tab.id, data);
  });
}

export function getQueryValue(queryString, name) {
  return queryString.find((p) => p.name === name)?.value;
}

/** @param {chrome.devtools.network.Request} result */
export async function parseResponse(result) {
  const [, key] = urlMatch.exec(result.request.url) ?? [];

  if (!key) {
    return;
  }

  const param1 = key === 'GetObject' ? 'type' : 'Query';
  const param2 = key === 'GetObject' ? 'name' : 'Parameters';

  return new Promise((resolve) => {
    result.getContent((content) => {
      const key2 = decodeURIComponent(
        getQueryValue(result.request.queryString, param1) ?? '_',
      );
      const key3 = decodeURIComponent(
        getQueryValue(result.request.queryString, param2) ?? '_',
      );

      resolve({
        key,
        key2,
        key3,
        content: JSON.parse(content),
      });
    });
  });
}
