export class DownloadQueue {
  constructor(onProcessItem) {
    this.onProcessItem = onProcessItem;
  }

  queue = [];

  processQueue = () => {
    if (this.queue.length) {
      const a = this.queue.shift();
      console.log(`Downloading ${a.getAttribute('download')}`);

      a.click();
      a.remove();

      setTimeout(() => {
        this.onProcessItem();
        this.processQueue();
      }, 500);
    }
  };

  download(cache) {
    // Add an item to the download queue for each key+subKey combination.
    Object.keys(cache).forEach((key) => {
      Object.keys(cache[key]).forEach((subKey) => {
        const dataStr = `data:text/json;charset=utf-8,${encodeURIComponent(
          JSON.stringify(cache[key][subKey], undefined, 2),
        )}`;

        const fileName = `${key}.${subKey}.json`;

        const downloadA = document.createElement('a');
        downloadA.setAttribute('href', dataStr);
        downloadA.setAttribute('download', fileName);
        document.body.appendChild(downloadA);

        this.queue.push(downloadA);
      });
    });

    // Process the queue
    console.log(`Downloading ${this.queue.length} files...`);
    this.processQueue();
  }
}
