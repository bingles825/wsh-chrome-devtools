import '../lib/react.production.min.js'
import '../lib/react-dom.production.min.js'
import '../lib/react-json-tree.min.js'

import { html } from './util.js'

// @ts-ignore
const R = React // Aliasing so we only have to @ts-ignore once
// @ts-ignore
const RD = ReactDOM

const extensionRoot = localStorage.getItem('WSH_EXTENSION_ROOT')
const overlayUrl = localStorage.getItem('WSH_OVERLAY_URL')

const config = {
  overlayUrl: overlayUrl ? extensionRoot + overlayUrl : '',
  overlayOffsetX: localStorage.getItem('WSH_OVERLAY_OFFSET_X') ?? '0',
  overlayOffsetY: localStorage.getItem('WSH_OVERLAY_OFFSET_Y') ?? '0',
  overlayOpacity: localStorage.getItem('WSH_OVERLAY_OPACITY') ?? '.5',
}

const overlayOptions = ['denials', 'ri', 'scheduler'].map((name) => ({
  name,
  value: `${extensionRoot}overlays/${name}.png`,
}))

const OverlayPanel = ({ config }) => {
  if (!config.overlayUrl) {
    return null
  }

  return html`<div
    className="wsh-overlay"
    style=${{
      backgroundImage: `url(${config.overlayUrl})`,
      left: config.overlayOffsetX,
      top: config.overlayOffsetY,
      opacity: config.overlayOpacity,
    }}
  />`
}

const ImageOverlay = ({ config: initialConfig }) => {
  const [showOverlay, setShowOverlay] = R.useState(false)

  const [config, setConfig] = R.useState(initialConfig)

  const onClick = R.useCallback(() => {
    setShowOverlay((v) => !v)
  }, [])

  const onUrlChange = R.useCallback(({ target }) => {
    setConfig((c) => ({ ...c, overlayUrl: target.value }))
    localStorage.setItem(
      'WSH_OVERLAY_URL',
      target.value.replace(extensionRoot, ''),
    )
  }, [])

  // if (!config) {
  //   return null
  // }

  return html`<${R.Fragment}>
    ${showOverlay && OverlayPanel({ config })}
    <div className="wsh-overlay-controls">
      <label>
        <input type="checkbox" onClick=${onClick} checked=${showOverlay}/>
        <span>Show Overlay</span>
      </label>
      <select value=${
        config.overlayUrl
      } onChange=${onUrlChange}><option></option>${overlayOptions.map(
    ({ name, value }) => html`<option value=${value}>${name}</option>`,
  )}</select>
    </div>
  </${R.Fragment}>`
}

RD.render(
  html`<${ImageOverlay} config=${config} />`,
  document.getElementById('wsh-content-script-app'),
)
