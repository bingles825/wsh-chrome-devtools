# WSH Dev Tools
Chrome extension providing dev tools for WhiteSpace Health stuff.

## Steps to install

1. Clone the repo, edit `mainfest.json` file and add path in `content_scripts`,`matches` to run the tool locally.(https://localhost:8081).
2. Go to chrome://extensions/ .
3. At the top right, turn on Developer mode.
4. Click Load unpacked.
5. Find and select the app.
6. Open a new tab in Chrome > click Apps > click the app or extension.


## Network Traffic Inspector

Shows `GetData` and `GetObject` network calls in a tree view. Before network calls will be tracked, you have to:

1. Open dev tools
2. Click `WSH Inspector` tab
3. Reload the page

![](docs/panel.png)

## Image Overlay
Set locaStorage `WSH_OVERLAY_URL` to `test.png`
TODO: Support other ways to pick url