console.log('Loading WSH Overlay.')

const overlayUrl = localStorage.getItem('WSH_OVERLAY_URL')
const overlayOffsetX = localStorage.getItem('WSH_OVERLAY_OFFSET_X') ?? '0'
const overlayOffsetY = localStorage.getItem('WSH_OVERLAY_OFFSET_Y') ?? '0'
const overlayOpacity = localStorage.getItem('WSH_OVERLAY_OPACITY') ?? '.3'

if (overlayUrl) {
  const div = document.createElement('div')
  div.className = 'wsh-overlay'
  div.style.backgroundImage = `url(${chrome.runtime.getURL('test.png')})`
  div.style.backgroundImage = overlayUrl
  div.style.backgroundRepeat = 'no-repeat'
  div.style.visibility = 'hidden'
  div.style.height = '100vh'
  div.style.left = overlayOffsetX
  div.style.opacity = overlayOpacity
  div.style.pointerEvents = 'none'
  div.style.position = 'fixed'
  div.style.top = overlayOffsetY
  div.style.width = '100vw'
  div.style.zIndex = '10000'
  document.body.appendChild(div)

  const controls = document.createElement('div')
  controls.className = 'wsh-overlay-controls'
  controls.style.position = 'fixed'
  controls.style.right = '0'
  controls.style.bottom = '0'
  controls.style.zIndex = '10000'
  document.body.appendChild(controls)

  const label = document.createElement('label')
  controls.appendChild(label)

  const checkbox = document.createElement('input')
  checkbox.type = 'checkbox'
  checkbox.checked = div.style.visibility === 'visible'
  checkbox.addEventListener('click', () => {
    div.style.visibility = checkbox.checked ? 'visible' : 'hidden'
  })
  label.appendChild(checkbox)

  const span = document.createElement('span')
  span.innerText = 'Show Overlay'
  label.appendChild(span)
}
